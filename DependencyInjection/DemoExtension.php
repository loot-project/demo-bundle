<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 06/03/20
 * Time: 16:12
 */

namespace Loot\Plugins\DemoBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

class DemoExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        // ... you'll load the files here later
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('twig', [
            'paths' => [
                __DIR__ . '/../Resources/views' => 'demo',
                __DIR__ . '/../Resources/views' => null
            ]
        ]);
    }
}