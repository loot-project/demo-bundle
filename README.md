# Demo Bundle

Demo bundle to extend default loot interface and display information about the demo

## Howto install

Clone the repository in the ```plugins``` directory of the loot application.

```
cd /path/to/loot/plugins
git clone https://gitlab.com/loot-project/demo-bundle.git ./DemoBundle
```