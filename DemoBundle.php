<?php
/**
 * Created by PhpStorm.
 * User: lolozere
 * Date: 06/03/20
 * Time: 15:55
 */

namespace Loot\Plugins\DemoBundle;

use Loot\Plugins\DemoBundle\DependencyInjection\DemoExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DemoBundle extends Bundle
{

    public function getContainerExtension()
    {
        return new DemoExtension();
    }

}